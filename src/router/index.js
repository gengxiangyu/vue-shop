import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome'
import Users from "../components/user/Users";

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    // 访问根路径,重定向到 /login 路径
    { path: '/', redirect: '/login'},
    { path: '/login', component: Login},
    // 访问home路径,重定向到 /welcome 路径,welcome为子路由规则
    { path: '/home', component: Home, redirect: '/welcome', children: [
        {path: '/welcome', component: Welcome},
        {path: '/users', component: Users},
      ]
    }
  ]
})

// 为路由对象，添加 beforeEach 路由导航守卫
router.beforeEach((to, from, next) => {
  // to 表示将要访问的路径
  // from 表示从哪个路径跳转而来
  // next 是一个函数,表示放行。next() 是直接放行, next('/login') 是强制跳转到该地址

  // 如果用户访问的 /login 路径就直接放行
  if (to.path === '/login') return next();

  // 获取Token
  const tokenStr = window.sessionStorage.getItem("token");
  // Token不存在则强制跳转到 /login 路径,会路由到登录页面
  if (!tokenStr) return next("/login");

  // 直接放行
  next();
})

export default router
