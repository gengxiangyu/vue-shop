import Vue from 'vue'
// 以下是按需导入Element组件
import { Button, Form, FormItem, Input, Message, MessageBox, Container, Header, Aside, Main,
    Menu, Submenu, MenuItem, Breadcrumb, BreadcrumbItem, Card, Row, Col, Table, TableColumn,
    Switch, Tooltip, Pagination, Dialog} from 'element-ui'

// 注册为全局组件
Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Menu)
Vue.use(Submenu)
Vue.use(MenuItem)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Card)
Vue.use(Row)
Vue.use(Col)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Switch)
Vue.use(Tooltip)
Vue.use(Pagination)
Vue.use(Dialog)
// 将 Message 消息提示组件挂载到 Vue 原型对象上,这样每个Vue的组件都可以通过 this 关键字直接访问到 $message
Vue.prototype.$message = Message
// 将 MessageBox 弹框组件中的 confirm 组件挂载到 Vue 原型对象上,这样每个Vue的组件都可以通过 this 关键字直接访问到 $confirm
Vue.prototype.$confirm = MessageBox.confirm