import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'

// 导入全局样式表
import './assets/css/global.css'
// 导入矢量图标样式,之后就可以在 class 上写 iconfont 图标类名即可使用,例如 class='iconfont icon-user'
import './assets/icons/iconfont.css'

// 导入Http请求包Axios
import axios from 'axios'
// 将Axios包挂载到Vue的原型对象上,这样每个Vue的组件都可以通过 this 关键字直接访问到 $http
Vue.prototype.$http = axios
// 配置后端接口的根路径
axios.defaults.baseURL = 'https://www.liulongbin.top:8888/api/private/v1/'
// Axios请求拦截器(所有接口请求之前都会先进入此拦截器)
axios.interceptors.request.use(config => {
  // 请求接口之前在头部携带上 Authorization,值为存储的Token
  config.headers.Authorization = window.sessionStorage.getItem('token');
  // 固定写法,最后必须返回 config
  return config;
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
